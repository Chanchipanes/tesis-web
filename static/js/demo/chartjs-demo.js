$(function () {

    var stackedData = {
        labels: ["Alumno 1", "Alumno 2", "Alumno 3", "Alumno 4", "Alumno 5", "Alumno 6", "Alumno 7"],
        datasets: [
            {
                label: "Actividad 1",
                backgroundColor: 'rgba(255,203,124,100)',
                borderColor: "rgba(255,187,69,100)",
                pointBorderColor: "#fff",
                data: [28, 48, 40, 19, 86, 27, 90]
            },
            {
                label: "Actividad 2",
                backgroundColor: 'rgba(245, 242, 142, 100)',
                borderColor: "rgba(241,245,93,96)",
                pointBorderColor: "#fff",
                data: [65, 59, 80, 81, 56, 55, 40]
            },
            {
                label: "Actividad 3",
                backgroundColor: 'rgba(140, 255, 161, 100)',
                borderColor: "rgba(108,255,145,100)",
                pointBorderColor: "#fff",
                data: [65, 59, 80, 81, 56, 55, 40]
            },
            
            {
                label: "Actividad 4",
                backgroundColor: 'rgba(115, 208, 235, 92)',
                borderColor: "rgba(84,194,235,92)",
                pointBorderColor: "#fff",
                data: [65, 59, 80, 81, 56, 55, 40]
            },
            
            {
                label: "Actividad 5",
                backgroundColor: 'rgba(173, 150, 255, 100)',
                borderColor: "rgba(151,117,255,100)",
                pointBorderColor: "#fff",
                data: [65, 59, 80, 81, 56, 55, 40]
            },
            
            {
                label: "Actividad 6",
                backgroundColor: 'rgba(255, 121, 104, 100)',
                borderColor: "rgba(255,98,81,100)",
                pointBorderColor: "#fff",
                data: [65, 59, 80, 81, 56, 55, 40]
            },
            {
                label: "Actividad 7",
                backgroundColor: 'rgba(118, 120, 255, 100)',
                borderColor: "rgba(103,104,255,100)",
                pointBorderColor: "#fff",
                data: [65, 59, 80, 81, 56, 55, 40]
            },
        ]
    };

    var stackedOptions = {
        responsive: true,
        scales: {
            xAxes:[{
                stacked:true,
            }],
            yAxes:[{
                stacked:true,
            }]

        },
        display: true
    };


    var ctx = document.getElementById("stacked").getContext("2d");
    new Chart(ctx, {type: 'horizontalBar', data: stackedData, options:stackedOptions});

    var barData = {
        labels: ["Alumno 1", "Alumno 2", "Alumno 3", "Alumno 4", "Alumno 5", "Alumno 6", "Alumno 7"],
        datasets: [
            {
                label: "Actividades completadas",
                backgroundColor: 'rgba(99,166,235,93)',
                borderColor: "rgba(80,154,235,92)",
                pointBorderColor: "#fff",
                data: [28, 48, 40, 19, 86, 27, 90]
            },
            {
                label: "Actividades no completadas",
                backgroundColor: 'rgba(255, 121, 104, 100)',
                borderColor: "rgba(255,98,81,100)",
                pointBorderColor: "#fff",
                data: [65, 59, 80, 81, 56, 55, 40]
            }
        ]
    };

    var barOptions = {
        responsive: true
    };


    var ctx2 = document.getElementById("horizontalBar").getContext("2d");
    new Chart(ctx2, {type: 'horizontalBar', data: barData, options:barOptions});

    var doughnutData = {
        labels: ["Actividad 1","Actividad 2","Actividad 3" , "Actividad 4", "Actividad 5", "Actividad 6", "Actividad 7"],
        datasets: [{
            data: [150,50,100,60,40,30,50],
            backgroundColor: ["#FFCB7C","#F5F28E","#8CFFA1", "#73D0EB","#AD96FF", "#FF7968", "#7678FF"]
        }]
    } ;


    var doughnutOptions = {
        responsive: true
    };


    var ctx4 = document.getElementById("doughnutChart").getContext("2d");
    new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});


});